import { SymbolKind, DocumentSymbol, DocumentSymbolProvider, Range, TextDocument, CancellationToken } from 'vscode';
import * as vscode from 'vscode';

export class LstDocumentSymbolProvider implements DocumentSymbolProvider {

    static readonly RegexLevelOne = vscode.workspace.getConfiguration('PunchedTapeX.Outline.RegexLevel').get('1');
    static readonly RegexLevelTwo = vscode.workspace.getConfiguration('PunchedTapeX.Outline.RegexLevel').get('2');
    static readonly RegexLevelThree = vscode.workspace.getConfiguration('PunchedTapeX.Outline.RegexLevel').get('3');

    regexLevelOne: RegExp = /^BD$/;
    regexLevelTwo: RegExp = /(BEGIN_)([A-Z_]+)/;
    regexLevelThree: RegExp = /(\()((Contour|KONTURNUMMER)\:\s?\d+)(\))/;

    constructor() {
        if (typeof LstDocumentSymbolProvider.RegexLevelOne === "string") {
            this.regexLevelOne = new RegExp(LstDocumentSymbolProvider.RegexLevelOne);
        }
        if (typeof LstDocumentSymbolProvider.RegexLevelTwo === "string") {
            this.regexLevelTwo = new RegExp(LstDocumentSymbolProvider.RegexLevelTwo);
        }
        if (typeof LstDocumentSymbolProvider.RegexLevelThree === "string") {
            this.regexLevelThree = new RegExp(LstDocumentSymbolProvider.RegexLevelThree);
        }
    }

    public provideDocumentSymbols(document: TextDocument, token: CancellationToken): Thenable<DocumentSymbol[]> {
        let path: any = require("path");
        const filename = path.basename(document.uri.fsPath);
        let programCounter: number = 1;
        return new Promise((resolve, reject) => {
            let activeNodeName: string = filename;
            let fileRange: Range = new Range(0, 0, 0, 0);
            let rootNode: DocumentSymbol = new DocumentSymbol(activeNodeName, "", SymbolKind.File, fileRange, fileRange);
            let matchesArray: RegExpMatchArray | null;
            let activeNodes: DocumentSymbol[] = [rootNode, rootNode, rootNode];
            let tempNode: DocumentSymbol;
            let symbols: DocumentSymbol[] = [];
            for (let i: number = 0; i < document.lineCount; i++) {
                let line: vscode.TextLine = document.lineAt(i);
                if (matchesArray = line.text.match(this.regexLevelOne)) {
                    activeNodeName = programCounter.toString();
                    programCounter++;
                    let description: string = filename;
                    tempNode = new DocumentSymbol(activeNodeName, description, SymbolKind.Module, line.range, line.range);
                    //activeNodes[0].children.push(tempNode);
                    symbols.push(tempNode);
                    activeNodes[1] = tempNode;
                }
                else if (matchesArray = line.text.match(this.regexLevelTwo)) {
                    if (matchesArray.length >= 3) { activeNodeName = matchesArray[2]; }
                    else { activeNodeName = matchesArray[0]; }
                    tempNode = new DocumentSymbol(activeNodeName, "", SymbolKind.Property, line.range, line.range);
                    activeNodes[1].children.push(tempNode);
                    activeNodes[2] = tempNode;
                }
                else if (matchesArray = line.text.match(this.regexLevelThree)) {
                    if (matchesArray.length >= 3) { activeNodeName = matchesArray[2]; }
                    else { activeNodeName = matchesArray[0]; }
                    tempNode = new DocumentSymbol(activeNodeName, "", SymbolKind.Key, line.range, line.range);
                    activeNodes[2].children.push(tempNode);
                }
            }
            resolve(symbols);
        });
    }
}
