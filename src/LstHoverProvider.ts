import { TextDocument, CancellationToken, ProviderResult, HoverProvider, Position, Hover, MarkdownString } from 'vscode';
import * as vscode from 'vscode';
import * as gCodeDictionary from "./GCode.json";
import * as cycleDictionary from "./NcCycles.json";

export interface CommandHash {
    [code: string]: ICommand;
}

export interface CycleHash {
    [code: string]: ICycle;
}

export interface IHoverText {
    getText(): MarkdownString;
}

interface ICommand {
    code: string;
    description: string;
}

class Command implements ICommand, IHoverText {
    code: string = "";
    description: string = "";

    constructor(code: string, description: string) {
        this.code = code;
        this.description = description;
    }

    getText() {
        return new MarkdownString(`**${this.code}** ${this.description}`);
    }
}

interface IParameter {
    name: string;
    description: string;
    values?: Array<IParameterValue>;
}

interface IParameterValue {
    value: string;
    description: string;
}

interface ICycle {
    cycle: string;
    description: string;
    parameter?: Array<any>;
}

class Cycle implements ICycle, IHoverText {
    cycle: string = "";
    description: string = "";
    parameter?: Array<IParameter> = new Array<IParameter>();

    constructor(code: string, description: string) {
        this.cycle = code;
        this.description = description;
    }

    addParameter(parameter: IParameter) {
        this.parameter?.push(parameter);
    }

    getText() {
        let paramaterlist: string = "";
        let parameterValueDescription: string = "";
        if (this.parameter) {
            parameterValueDescription += "### Parameter\n\n";
            this.parameter.forEach((element, index) => {
                if (index === 0) { paramaterlist += element.name; }
                else { paramaterlist += ", " + element.name; }
                parameterValueDescription += `* ${element.name}:`;
                if (element.description.length > 0) {
                    parameterValueDescription += ` *${element.description}*`;
                }
                parameterValueDescription += "\n";
                if (element.values) {
                    element.values.forEach(entry => {
                        parameterValueDescription += "  * " + entry.value + ": " + entry.description + "\n";
                    });
                }
            });
        }
        return new MarkdownString(`**${this.cycle}**(${paramaterlist})

### Description

${this.description}

${parameterValueDescription}
`);
    }
}

export class LstHoverProvider implements HoverProvider {
    commands: CommandHash = gCodeDictionary as CommandHash;
    cycles: CycleHash = cycleDictionary as CycleHash;
    static readonly useHoverHelp = vscode.workspace.getConfiguration('PunchedTapeX').get('UseHoverHelp') as boolean;

    public provideHover(document: TextDocument, position: Position, token: CancellationToken): ProviderResult<Hover> {
        if (!LstHoverProvider.useHoverHelp) { return; }
        const range = document.getWordRangeAtPosition(position);
        const word = document.getText(range);

        let description = this.commands[word] as Command;
        if (description) {
            let command: Command = new Command(description.code, description.description);
            return new Hover(command.getText());
        }
        else {
            let description2: Cycle
                = this.cycles[word] as Cycle;
            if (description2) {
                let command: Cycle = new Cycle(description2.cycle, description2.description);
                if (description2.parameter) {
                    description2.parameter.forEach(element => {
                        command.addParameter(element);
                    });
                }
                return new Hover(command.getText());
            }
        }
    }
}
