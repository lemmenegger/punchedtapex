import * as vscode from 'vscode';
import { LstHeadTableEntry } from './LstHeadTableEntry';
/**
 * Manages LST Head Table webview panels
 */
export class LstHeadTablePanel {
    static readonly RegexTitle = vscode.workspace.getConfiguration('PunchedTapeX.Outline.RegexLevel').get('2');
    regexTitle: RegExp = /(BEGIN_)([A-Z_]+)/;
    /**
	 * Track the currently panel. Only allow a single panel to exist at a time.
	 */
    public static currentPanel: LstHeadTablePanel | undefined;

    public static readonly viewType = 'headTable';

    private readonly _panel: vscode.WebviewPanel;
    private _disposables: vscode.Disposable[] = [];

    public static createOrShow() {
        const source = vscode.window.activeTextEditor;
        const column = vscode.ViewColumn.Two;

        // If we already have a panel, show it.
        if (LstHeadTablePanel.currentPanel && source) {
            LstHeadTablePanel.currentPanel._panel.reveal(column, true);
            LstHeadTablePanel.currentPanel.updateData(source);
            return;
        }

        // Otherwise, create a new panel.
        const panel = vscode.window.createWebviewPanel(
            LstHeadTablePanel.viewType,
            'Head Table',
            column || vscode.ViewColumn.Two,
            {
                // Enable javascript in the webview
                //enableScripts: true,
                // And restrict the webview to only loading content from our extension's `media` directory.
                //localResourceRoots: [vscode.Uri.file(path.join(extensionPath, 'media'))]
            }
        );

        if (source) {
            LstHeadTablePanel.currentPanel = new LstHeadTablePanel(panel);
            LstHeadTablePanel.currentPanel.updateData(source);
        }
    }

    private constructor(panel: vscode.WebviewPanel) {
        if (typeof LstHeadTablePanel.RegexTitle === "string") {
            this.regexTitle = new RegExp(LstHeadTablePanel.RegexTitle);
        }
        this._panel = panel;
        // Listen for when the panel is disposed
        // This happens when the user closes the panel or when the panel is closed programatically
        this._panel.onDidDispose(() => this.dispose(), null, this._disposables);
    }

    public updateData(editor: vscode.TextEditor) {
        let title: string = "";
        let array = new Array<LstHeadTableEntry>();
        if (editor.selection) {
            let document = editor.document;
            let matchesArray: RegExpMatchArray | null;
            let activeLine = editor.selection.active.line;
            let line: vscode.TextLine = document.lineAt(activeLine);
            // find active HeadTable
            for (let i: number = activeLine; i > 0; i--) {
                line = document.lineAt(i);
                if (matchesArray = line.text.match(this.regexTitle)) {
                    if (matchesArray.length >= 3) { title = matchesArray[2]; }
                    break;
                }
            }
            // fill HeadTable entries
            let entrySummary = document.lineAt(line.lineNumber + 2).text;
            let numberOfEntries: number = parseInt(entrySummary.split(",", 3)[2]);
            let startLineNumber = line.lineNumber + 3;
            let endLineNumber = startLineNumber + numberOfEntries;
            for (let i: number = startLineNumber; i < endLineNumber; i++) {
                line = document.lineAt(i);
                let entryArray = line.text.split(",", 11);
                let id: number = parseInt(entryArray[3]);
                let description: string = entryArray[7].trim().replace(/^'|'$/g, "");
                let unit: string = entryArray[9].trim().replace(/^'|'$/g, "");
                let entry = new LstHeadTableEntry(id, description, unit);
                array.push(entry);
            }
            // fill HeadTable values
            let valueSummary = document.lineAt(line.lineNumber + 2).text;
            let numberOfValues: number = parseInt(valueSummary.split(",", 3)[2]);
            startLineNumber = line.lineNumber + 3;
            endLineNumber = startLineNumber + numberOfValues;
            let valueArray = new Array<string>();
            for (let i: number = startLineNumber; i < document.lineCount; i++) {
                let ncProgramm = false;
                line = document.lineAt(i);
                let text = line.text.trim();
                if (text.match(/^C$/)) { break; }
                if (matchesArray = text.match(/(^DA,)(.*)(\s*,$)/)) {
                    valueArray = matchesArray[2].split(",");
                }
                else if (matchesArray = text.match(/(^\*\s*)(.*)(\s*,$)/)) {
                    let tempArray = matchesArray[2].split(",");
                    valueArray = valueArray.concat(tempArray);
                }
                else if (matchesArray = text.match(/(^\*\s*)(.*)/)) {
                    let tempArray = matchesArray[2].split(",");
                    valueArray = valueArray.concat(tempArray);
                }
                else if (matchesArray = text.match(/(^\-\s*)(.*)(\s*,$)/)) {
                    let tempArray = matchesArray[2].split(",");
                    let lastElementPreviousLine = valueArray.pop();
                    let firstElementCurrentLine = tempArray[0];
                    if (firstElementCurrentLine && lastElementPreviousLine) {
                        tempArray[0] = lastElementPreviousLine.slice(0, -1).concat(firstElementCurrentLine.slice(1));
                    }
                    valueArray = valueArray.concat(tempArray);
                }
                else if (matchesArray = text.match(/(^DA,)(.*)/)) {
                    valueArray = matchesArray[2].split(",");
                }
                if (text.match(/^[DA|\*|-].*[^,]$/)) {
                    if (array.length !== valueArray.length) {
                        throw new Error('number of values do not match to the number of entries!');
                    }
                    valueArray.forEach((value, index) => {
                        array[index].values.push(value.replace(/^'|'$/g, ""));
                    });
                }
            }
        }

        const webview = this._panel.webview;
        this._panel.title = title;
        this._panel.webview.html = this._getHtmlForWebview(webview, title, array);
    }

    public dispose() {
        LstHeadTablePanel.currentPanel = undefined;

        // Clean up our resources
        this._panel.dispose();

        while (this._disposables.length) {
            const x = this._disposables.pop();
            if (x) {
                x.dispose();
            }
        }
    }

    private _getHtmlForWebview(webview: vscode.Webview, title: string, tableValues: Array<LstHeadTableEntry>) {
        let htmlTable: string = "";
        if (tableValues.length > 0) {
            let numberOfValues = tableValues[0].values.length;
            let htmlTableHeader: string = "";
            let headers = new Array<string>(2 + numberOfValues).fill("");
            let htmlTableBody: string = "";
            tableValues.forEach(entry => {
                htmlTableBody += `<tr>\n<td>${entry.id}</td><td>${entry.description}</td>`;
                entry.values.forEach((value) => {
                    htmlTableBody += `<td class\="right">${value}&nbsp;${entry.unit}</td>`;
                });
                htmlTableBody += `\n</tr>`;
            });
            headers[0] = "id";
            headers[1] = "description";
            htmlTableHeader = `<tr>\n`;
            headers.forEach((header, index) => {
                if (header) {
                    htmlTableHeader += `<th>${header}</th>`;
                }
                else {
                    htmlTableHeader += `<th class\="right">value ${index - 1}</th>`;
                }
            });
            htmlTableHeader += `\n<tr>`;
            htmlTable = `<table>\n${htmlTableHeader}\n${htmlTableBody}\n</table>`;
        }

        return `<!DOCTYPE html>
            <html lang="en">
            <head>
                <meta charset="UTF-8">
                <meta name="viewport" content="width=device-width, initial-scale=1.0">
                <title>${title}</title>
                <style>
                    table {border-collapse: collapse;}
                    td {
                        padding: 5px;
                        border-top: 1px solid gray;
                        }
                    th {text-align: left;}
                    .right {text-align: right;}
                </style>
            </head>
            <body>
                <h1>${title}</h1>
                ${htmlTable}
            </body>
            </html>`;
    }
}