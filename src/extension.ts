// The module 'vscode' contains the VS Code extensibility API
// Import the module and reference it with the alias vscode in your code below
import * as vscode from 'vscode';
import { languages } from 'vscode';
import { LstDocumentSymbolProvider } from './LstDocumentSymbolProvider';
import { LstHoverProvider } from './LstHoverProvider';
import { LstHeadTablePanel } from './LstHeadTableView';

// this method is called when your extension is activated
// your extension is activated the very first time the command is executed
export function activate(context: vscode.ExtensionContext) {
    let lstLangId: string = 'lst';
    let lstSelector: vscode.DocumentSelector = {scheme: 'file', language: lstLangId };

    context.subscriptions.push(
        vscode.commands.registerCommand('Lst.HeadTable', () => {
            LstHeadTablePanel.createOrShow();
        })
    );

    context.subscriptions.push(languages.registerDocumentSymbolProvider(
        lstSelector, new LstDocumentSymbolProvider()
    ));

    vscode.languages.registerHoverProvider(
        lstSelector, new LstHoverProvider()
    );

    vscode.languages.setLanguageConfiguration(lstLangId, {wordPattern: /([A-Z][\-0-9.]+)|(TC_[A-Z_]+)/});
    vscode.commands.executeCommand('outline.focus');
}

// this method is called when your extension is deactivated
export function deactivate() { }
