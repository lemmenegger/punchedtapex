export class LstHeadTableEntry {
    id: number;
    description: string;
    unit: string;
    values: Array<string>;
    constructor(id: number, description: string, unit: string) {
        this.id = id;
        this.description = description;
        this.unit = unit;
        this.values = new Array();
    }
}