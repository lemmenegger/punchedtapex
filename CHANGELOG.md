# Change Log

All notable changes to the "punchedtapex" extension will be documented in this file.

## [Unreleased]

## [0.0.2] - 2020-01-08

### Added

- new icon
- add to offline installer-package
- add license

## [0.0.1] - 2020-01-06

- First implemenation of syntax highlighting, outline, conversion of the header tables and some explanations on nc-commands and nc-cycles 