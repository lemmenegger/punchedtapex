# PunchedTapeX

The extension should help to make the "punched tape" a bit more readable.

## Features

The following functions helps when working on NC files (`*.lst`):

* syntax highlighting
* outline over the whole file (configured with three regular expressions)
* conversion of the header tables into a readable form (Keybinding `[F12]`)
* explanations on nc-commands and nc-cycles (on hover)

![screen](https://gitlab.com/lemmenegger/punchedtapex/raw/master/img/screen.png "Extension in action")

## Requirements

No additional requirements

## Install this extension

... until this extension is shared with the world 😉
* To start using your extension with Visual Studio Code copy it into the `<user home>/.vscode/extensions` folder and restart Code.

## Extension Settings

This extension contributes the following settings:

* `PunchedTapeX.UseHoverHelp`: enable/disable to show informations about the current nc-command or nc-cycle
* `PunchedTapeX.Outline.RegexLevel.1`: The regular expression to find level 1 for the ouline tree.
* `PunchedTapeX.Outline.RegexLevel.2`: The regular expression to find level 3 for the ouline tree. For the name the second group match is taken.
* `PunchedTapeX.Outline.RegexLevel.3`: The regular expression to find level 3 for the ouline tree. For the name the second group match is taken.

## Known Issues

* No support for transforming head table *TOPS_DATA*
* just a few number of nc-cycles in dictionary
* available in English only
* some "technical debts" ...

## Release Notes

### 0.0.2

some fixes and a add to offline installer

### 0.0.1

first test version
